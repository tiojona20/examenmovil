package com.example.examencalculadora;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CalculadoraActivity extends AppCompatActivity {

    private Calculadora cal;
    private TextView lblUsuario, lblResultado;
    private EditText txtNumA, txtNumB;
    private Button btnSuma, btnResta, btnMulti, btnDiv, btnBack, btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_calculadora);

        iniciarComponentes();

        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNumA.setText("");
                txtNumB.setText("");
                lblResultado.setText("");
            }
        });

        this.btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion('+');
            }
        });

        this.btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion('-');
            }
        });

        this.btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion('*');
            }
        });

        this.btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion('/');
            }
        });

        this.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes() {
        lblUsuario = findViewById(R.id.lblUsuario);
        lblResultado = findViewById(R.id.lblResultado);
        txtNumA = findViewById(R.id.txtNumA);
        txtNumB = findViewById(R.id.txtNumB);
        btnSuma = findViewById(R.id.btnSuma);
        btnResta = findViewById(R.id.btnResta);
        btnMulti = findViewById(R.id.btnMulti);
        btnDiv = findViewById(R.id.btnDiv);
        btnBack = findViewById(R.id.btnBack);
        btnLimpiar = findViewById(R.id.btnLimpiar);
    }

    private void realizarOperacion(char operador) {
        try {
            float numA = Float.parseFloat(txtNumA.getText().toString());
            float numB = Float.parseFloat(txtNumB.getText().toString());
            cal = new Calculadora(numA, numB);

            float resultado = 0;
            switch (operador) {
                case '+':
                    resultado = cal.suma();
                    break;
                case '-':
                    resultado = cal.resta();
                    break;
                case '*':
                    resultado = cal.multiplicar();
                    break;
                case '/':
                    resultado = cal.division();
                    break;
            }
            lblResultado.setText(String.valueOf(resultado));
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Ingrese números válidos", Toast.LENGTH_SHORT).show();
        } catch (DivisionByZeroException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
